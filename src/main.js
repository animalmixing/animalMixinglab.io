import Vue from 'vue'
import App from './App.vue'
import VueYoutubeEmbed from "vue-youtube-embed";
import 'keen-ui/dist/keen-ui.css';

Vue.use(VueYoutubeEmbed);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
